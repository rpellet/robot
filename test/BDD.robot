
*** Settings ***
Resource	squash_resources.resource
Library    SeleniumLibrary

*** Keywords ***
Test Setup
    Open Browser    ${url}    ${browser}

Test Teardown
    Close Browser

*** Test Cases ***
curalogin
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	When L'utilisateur souhaite prendre un rendez-vous
	Then La page de connexion s'affiche
	When L'utilisateur se connecte
	Then L'utilisateur est connecté sur la page de prise de rendez-vous

	[Teardown]	Test Teardown